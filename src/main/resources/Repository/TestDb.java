package Repository;

import java.util.ArrayList;
import java.util.List;

import User.EnumerationValue;
import User.RolesPermission;
import User.User;
import User.Roles;

public class TestDb {

	List<User> users;
	List<RolesPermission> userPermission;
	List<Role> userRoles;
	List<EnumerationValue> enumValues;
	
	
	public TestDb() {
		
		users = new ArrayList<User>();
		userPermission = new ArrayList<RolesPermission>();
		userRoles = new ArrayList<Role>();
		enumValues = new ArrayList<EnumerationValue>();
		
	}
}