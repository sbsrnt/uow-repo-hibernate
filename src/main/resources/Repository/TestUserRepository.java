package Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import UoW.UnitOfWorkRepository;
import User.Entity;
import User.EntityState;
import User.RolesPermission;
import User.User;
import User.Roles;

public class TestUserRepository implements UserRepository, UnitOfWorkRepository {
	
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	
	Database db = new Database();

	@Override
	public User withId(int id) {
		
	User user = null;
		
		for(User v: db.users)
		{
			if (v.getId() == id) {
				user = v;
			}
		}
		return user;
	}

	@Override
	public void add(User entity) {
		entity.setState(EntityState.NEW);
		db.users.add(entity);
		
	}

	@Override
	public void delete(User entity) {
		entity.setState(EntityState.DELETED);
		db.users.remove(entity);
		
	}

	@Override
	public void modify(User entity) {
		entity.setState(EntityState.MODIFIED);
		User user = null;
		
		for(User v: db.users)
		{
			if(v.getId() == entity.getId()) {
				user = v;
			}
		}
		// jakies operacje
	}

	@Override
	public int count() {
		return db.users.size();
	}

	@Override
	public User withLogin(String login) {

		User user = null;
		
		for(User v: db.users)
		{
			if (v.getLogin() == login) {
				user = v;
			}
		}
		return user;
	}

	@Override
	public User withLoginAndPassword(String login, String password) {
		
		User user = null;
		
		for(User v: db.users)
		{
			if (v.getLogin() == login && v.getPassword() == password) {
				user = v;
			}
		}
		return user;
	}

	@Override
	public void setupPermission(User user) {
		
		Roles role = new Roles();
		RolesPermission rolesPermission = new RolesPermission();
		rolesPermission.setPermissionId(1);
		rolesPermission.setRoleId(1);
		role.addRolePermission(rolesPermission);
		role.addUser(user);
		role.setUserId(0);
		role.setRoleId(rolesPermission.getRoleId());
		user.addRole(role);
		rolesPermission.addRole(role);
		
	}

	@Override
	public void persistAdd(Entity entity) {	
		try {
			setUpInsertQuery((User)entity);
			insert.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void persistRemove(Entity entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void persistUpdate(Entity entity) {
		try {
			setUpUpdateQuery((User)entity);
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void setUpUpdateQuery(User entity) throws SQLException {
	}
	protected void setUpInsertQuery(User entity) throws SQLException {
	}

}