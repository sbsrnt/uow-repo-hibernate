package Repository;

public class TestRepositoryCatalog implements RepositoryCatalog {

	
	private Database db = new Database();
	
	@Override
	public EnumerationValueRepository enumerations() {
		return (EnumerationValueRepository) db.enumValues;
	}

	@Override
	public UserRepository users() {	
		return (UserRepository) db.users;
	}

}