/* SQL */
/* 
   create table USER (
   id INT NOT NULL auto_increment,
   login VARCHAR(20) default NULL,
   password  VARCHAR(20) default NULL,
   role  VARCHAR(20)  default NULL,
   PRIMARY KEY (id)
	);
 */
import java.util.List; 
import java.util.Date;
import java.util.Iterator; 
 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class UserSettings {
	   private static SessionFactory factory; 
	   public static void main(String[] args) {
	      try{
	         factory = new Configuration().configure().buildSessionFactory();
	      }catch (Throwable ex) { 
	         System.err.println("Fail session." + ex);
	         throw new ExceptionInInitializerError(ex); 
	      }
	      UserSettings US = new UserSettings();

	      Integer uID1 = US.addUser("nick1", "qwerty", "admin");
	      Integer uID2 = US.addUser("nick2", "12345", "user");
	      Integer uID3 = US.addUser("nick3", "qwe.123", "user");

	      US.listUsers();
	   }
	   /* stworz usera */
	   public Integer addUser(String login, String password, Role role){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      Integer userID = null;
	      try{
	         tx = session.beginTransaction();
	         User user = new User(login, password, role);
	         userID = (Integer) session.save(user); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	      return userID;
	   }
	   /* pokaz dane userow*/
	   public void listUsers( ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         List users = session.createQuery("FROM User").list(); 
	         for (Iterator iterator = 
	                           users.iterator(); iterator.hasNext();){
	            User user = (User) iterator.next(); 
	            System.out.print("First Name: " + user.getLogin()); 
	            System.out.print("Last Name: " + user.getPassword()); 
	            System.out.println("Role: " + user.getRole()); 
	         }
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	   }
	   /* update usera */
	   public void updateUser(Integer UserID, int role ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         User user = 
	                    (User)session.get(User.class, UserID); 
	         user.setRole( role );
			 session.update(user); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	   }
	   /* usuwanie usera */
	   public void deleteUser(Integer UserID){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         User user = 
	                   (User)session.get(User.class, UserID); 
	         session.delete(user); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	   }
	}